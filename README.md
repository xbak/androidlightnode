# Android
## Installation
L'installation de cette branche se fait automatiquement lors de la récupération du projet à
l'aide de la commande git:
```
https://gitlab.com/ausy_rd/biotv2_release/android.git
```

## Usage
Avant d'entâmer ce processus, assurez-vous que la node principale soit correctement installée
et en marche. **Dans le cadre de la node Android, il faut également s'assurer qu'un contrat
existe sur la blockchain visée**. Dans le cas contraire, les fonctionnalités de suivi de
progression de la *blockchain* seront toujours disponibles, mais l'application aura une erreur
fatale si un appel ou une transaction est effectué.

1. Dans le fichier app/src/main/java/com/example/ethereumpassivenode/ContractIdentifier.java,
remplacer la valeur de l'attribut de classe *address* par l'adresse du contrat sur la
*blockchain*. Si le contrat a été modifié, remplacez également son abi.
![Address & ABI](./screenshots/20200129_adresse_abi.png)

2. Dans le fichier app/src/main/java/com/example/ethereumpassivenode/NodeConfiguration.java,
remplacer la valeur de l'attribut *enode* par l'enode du noeud principal
![Enode](./screenshots/20200129_enode.png)

3. Compiler ensuite le programme sur la machine voulue attachée par USB à l'ordinateur et
dont le mode *déboggage USB* est sélectionné.
  * Dans la barre d'outil en haut à gauche, sélectionner *Google Pixel XL* et appuiller sur la
flèche verte.
![Play](./screenshots/20200129_play.png)


