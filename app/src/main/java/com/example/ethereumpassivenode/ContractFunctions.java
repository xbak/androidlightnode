package com.example.ethereumpassivenode;

import org.ethereum.geth.Address;
import org.ethereum.geth.CallOpts;
import org.ethereum.geth.Context;
import org.ethereum.geth.Geth;
import org.ethereum.geth.Interface;
import org.ethereum.geth.Interfaces;
import org.ethereum.geth.KeyStore;
import org.ethereum.geth.Nonce;
import org.ethereum.geth.Signer;
import org.ethereum.geth.TransactOpts;
import org.ethereum.geth.Transaction;

public interface ContractFunctions {
    ContractIdentifier contractIdentifier = ContractIdentifier.getInstance();
    ClientIdentifier clientIdentifier = ClientIdentifier.getInstance();
    NodeConfiguration nodeConfiguration = NodeConfiguration.getInstance();

    //compte à partir duquel se fait la transaction
    //toujours virer quelques ethers au lancement de l'appareil mobile
    KeyStore keystore = new KeyStore(
            "/data/user/0/com.example.ethereumpassivenode/files/.ethereum/Gethdroid/keystore",
            Geth.LightScryptN, Geth.LightScryptP);
    String PASSPHRASE = "passphrase";

    //méthode de signature d'une transaction
    Signer signer = new Signer() {
        @Override
        public Transaction sign(Address address, Transaction transaction) throws Exception {
            keystore.timedUnlock(keystore.getAccounts().get(0), PASSPHRASE, 15000);
            byte[] signature = keystore.signHash(keystore.getAccounts().get(0).getAddress(),
                    transaction.getSigHash().getBytes());
            //transaction.withSignature(x, y) où x = signature; y = chainId du genesisBlock
            return transaction.withSignature(signature, Geth.newBigInt(1506754));
        }
    };

    //déverrouillage du compte
    default void accountUnlocker() throws Exception{
        if(keystore.getAccounts().size() == 0){
            keystore.newAccount(PASSPHRASE);
        }
        //keystore.timedUnlock(keystore.getAccounts().get(0), PASSPHRASE, 15000);
        keystore.unlock(keystore.getAccounts().get(0), PASSPHRASE);
    }

    //paramètres de transactions
    default TransactOpts getTransactOps(Context ctx) throws Exception {
        TransactOpts transactOpts = new TransactOpts();
        transactOpts.setFrom(keystore.getAccounts().get(0).getAddress());
        transactOpts.setSigner(signer);
        transactOpts.setValue(Geth.newBigInt(0));
        transactOpts.setGasPrice(clientIdentifier.ethereumClient.suggestGasPrice(ctx));
        transactOpts.setGasLimit(clientIdentifier.ethereumClient.getBlockByNumber(ctx, -1).getGasLimit());
        transactOpts.setNonce(clientIdentifier.ethereumClient.getPendingNonceAt(ctx, keystore.getAccounts().get(0).getAddress()));
        //transactOpts.setNonce(clientIdentifier.ethereumClient.getTransactionCount(ctx, clientIdentifier.ethereumClient.getBlockByNumber(ctx, -1).getHash()));
        //transactOpts.setNonce(clientIdentifier.ethereumClient.getNonceAt(ctx, keystore.getAccounts().get(0).getAddress(), -1));
        transactOpts.setContext(ctx);


        return transactOpts;
    }

    //paramètres d'appels
    default CallOpts getCallOpts(Context ctx) throws Exception {
        CallOpts callOpts = new CallOpts();
        callOpts.setGasLimit(clientIdentifier.ethereumClient.getBlockByNumber(ctx, -1).getGasLimit());
        callOpts.setPending(false);
        callOpts.setContext(ctx);

        return callOpts;
    }

    //----

    //fonction contrat: retrieveStandards()
    default Interfaces retrieveStandards(Context ctx) throws Exception {
        Interfaces retrieveStandardsResults = new Interfaces(8);
        for(int i = 0; i < retrieveStandardsResults.size(); i++) {
            Interface anInterface = new Interface();
            anInterface.setDefaultUint16();
            retrieveStandardsResults.set(i, anInterface);
        }

        Geth.bindContract(contractIdentifier.address, contractIdentifier.abi,
                clientIdentifier.ethereumClient).call(getCallOpts(ctx),
                retrieveStandardsResults, "retrieveStandards", Geth.newInterfaces(0));

        return retrieveStandardsResults;
    }

    //fonction contrat: retrieveDataOneByOne()
    //retourner toujours la valeur au dernier bloc
    default Interfaces retrieveDataOneByOne(Context ctx) throws Exception {
        Interfaces retrieveDataOneByOne = new Interfaces(8);

        for(int i = 0; i < 3; i++) {
            Interface anInterface = new Interface();
            anInterface.setDefaultString();
            retrieveDataOneByOne.set(i, anInterface);
        }

        Interface anInterface = new Interface();
        anInterface.setDefaultUint64();
        retrieveDataOneByOne.set(3, anInterface);

        for(int i = 4; i < retrieveDataOneByOne.size(); i++) {
            Interface anInterface1 = new Interface();
            anInterface1.setDefaultUint16();
            retrieveDataOneByOne.set(i, anInterface1);
        }

        Interfaces retrieveDataOneByOneParam = new Interfaces(1);
        Interface retrieveDataOneByOneParamType = new Interface();
        retrieveDataOneByOneParamType.setUint16(Geth.newBigInt(-1));
        retrieveDataOneByOneParam.set(0, retrieveDataOneByOneParamType);

        Geth.bindContract(contractIdentifier.address, contractIdentifier.abi,
                clientIdentifier.ethereumClient).call(getCallOpts(ctx),
                retrieveDataOneByOne, "retrieveDataOneByOne", retrieveDataOneByOneParam);

        return retrieveDataOneByOne;

    }

    //fonction contrat: updateStandardsAir
    default Transaction updateStandardsAir(Context ctx, int coLow, int coHigh, int co2Low,
                                           int co2High) throws Exception {
        int[] arr = new int[] {coLow, coHigh, co2Low, co2High};
        Interfaces updateStandardsAirParams = new Interfaces(4);

        for(int i = 0; i < updateStandardsAirParams.size(); i++){
            Interface anInterface = new Interface();
            anInterface.setUint16(Geth.newBigInt(arr[i]));
            updateStandardsAirParams.set(i, anInterface);
        }

        accountUnlocker();
        Transaction transaction = Geth.bindContract(contractIdentifier.address,
                contractIdentifier.abi,
                clientIdentifier.ethereumClient).transact(getTransactOps(ctx),
                "updateStandardsAir", updateStandardsAirParams);

        return transaction;
    }

    //fonction contrat: updateStandardsWater
    default Transaction updateStandardsWater(Context ctx, int phLow, int phHigh, int turbLow,
                                             int turbHigh) throws Exception {
        int[] arr = new int[] {phLow, phHigh, turbLow, turbHigh};
        Interfaces updateStandardsWaterParams = new Interfaces(4);

        for(int i = 0; i < updateStandardsWaterParams.size(); i++) {
            Interface anInterface = new Interface();
            anInterface.setUint16(Geth.newBigInt(arr[i]));
            updateStandardsWaterParams.set(i, anInterface);
        }

        accountUnlocker();
        Transaction transaction = Geth.bindContract(contractIdentifier.address,
                contractIdentifier.abi,
                clientIdentifier.ethereumClient).transact(getTransactOps(ctx),
                "updateStandardsWater", updateStandardsWaterParams);

        return transaction;
    }
}
