package com.example.ethereumpassivenode;

import android.os.AsyncTask;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class Hostname extends AsyncTask<Void, Void, String> {

    @Override
    protected String doInBackground(Void... voids) {
        String hostName = null;
        try {
            hostName = InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        return hostName;
    }
    
    protected void onPostExecute() {
        //
    }
}
