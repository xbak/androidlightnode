package com.example.ethereumpassivenode;

import org.ethereum.geth.EthereumClient;
import org.ethereum.geth.Geth;

class ClientIdentifier {
    private static ClientIdentifier ourInstance;

    static {
        try {
            ourInstance = new ClientIdentifier();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    static ClientIdentifier getInstance() {
        return ourInstance;
    }

    //peering avec la machine distante
    EthereumClient ethereumClient = Geth.newEthereumClient("ws://" + Network.primaryIpAddress + ":" + Network.wsIpPort);

    private ClientIdentifier() throws Exception {
    }
}
