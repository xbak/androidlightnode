package com.example.ethereumpassivenode;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.kenai.jffi.Main;

import org.ethereum.geth.*;
import org.web3j.abi.datatypes.Bool;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.xml.transform.Result;

public class MainActivity extends AppCompatActivity implements ContractFunctions {
    Context ctx = new Context();

    @RequiresApi(api = Build.VERSION_CODES.O)
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setTitle("Client léger Android");
        final TextView blockchainProgressionView = findViewById(R.id.blockchainProgressionView);

        Button[] buttons = new Button[] {
                (Button) findViewById(R.id.blockchainProgressionButton),
                (Button) findViewById(R.id.ioInterfaceButton),
                (Button) findViewById(R.id.retrieveStandardsButton),
                (Button) findViewById(R.id.retrieveDataOneByOneButton),
                (Button) findViewById(R.id.updateStandardsAirButton),
                (Button) findViewById(R.id.updateStandardsWaterButton),
        };
        for(int i = 0; i < buttons.length; i++){
            buttons[i].setOnClickListener(this::onClick);
        }

        File directory = new File(getFilesDir() + "/.ethereum/Gethdroid/GethDroid/lightchaindata");
        directory.listFiles(new FilenameFilter() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public boolean accept(File directory, String s) {
                if(s.endsWith(".ldb") || s.endsWith(".log")){
                    try {
                        Files.delete(Paths.get(getFilesDir() + "/.ethereum/Gethdroid/GethDroid/lightchaindata/" + s));
                        } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                return true;
            }
        });

        for(int i = 0; i < Network.ipAddressList.length; i++){
            AsyncTask<String, Void, Boolean> network = new Network().execute(Network.ipAddressList[i], Network.rpcIpPort);
            try {
                if(network.get()){
                    System.out.println("true");
                    network.cancel(true);
                    Network.setPrimaryIpAddress(Network.ipAddressList[i]);
                    Network.setBootstrapNode(Network.ipAddressList[i], Network.webServerIpPort);
                    break;
                }
                System.out.println("false");
            } catch (ExecutionException e) {
                //e.printStackTrace();
            } catch (InterruptedException e) {
                //e.printStackTrace();
            }
        }

        try {
            Node node = Geth.newNode(getFilesDir() + "/.ethereum/Gethdroid",
                    nodeConfiguration.nodeConfig);
            node.start();

            NodeInfo info = node.getNodeInfo();

            blockchainProgressionView.append("Nom: " + info.getName() + "\n");
            blockchainProgressionView.append("Adresse: " + info.getListenerAddress() + "\n");
            try {
                blockchainProgressionView.append("Dernier bloc connu: #" +
                        clientIdentifier.ethereumClient.getBlockByNumber(ctx, -1).getNumber() + ", synchronisation...\n\n");
            } catch (Exception e) {
                blockchainProgressionView.append("Echec de connection à la chaine de blocs !");
            }

            blockchainProgressionView.append("Dernier bloc: " +
                    ClientIdentifier.getInstance().ethereumClient.getBlockByNumber(ctx, -1).getNumber() +
                    ", synchronisation...\n");
            
            //suivi d'évolution de chaine de blocs
            NewHeadHandler handler = new NewHeadHandler() {
                @Override public void onError(String error) { }
                @Override public void onNewHead(final Header header) {
                    MainActivity.this.runOnUiThread(new Runnable() {
                        public void run() { blockchainProgressionView.getEditableText().insert((114 + String.valueOf(header.getNumber()).length()),
                                "#" + header.getNumber() + ": " +
                                        header.getHash().getHex().substring(0, 10) + ".\n");
                        if(blockchainProgressionView.getText().length() > 550) {
                            blockchainProgressionView.getEditableText().delete(
                                    blockchainProgressionView.getText().length() -
                                            (15 + String.valueOf(header.getNumber()).length()),
                                    blockchainProgressionView.getText().length());
                        }
                        }
                    });
                }
            };

            //error: subscribeNewHead not available via http://, only ws://.
            clientIdentifier.ethereumClient.subscribeNewHead(ctx, handler, 16);

            //créer compte si aucun n'existe
            if(keystore.getAccounts().size() == 0){
                keystore.newAccount(PASSPHRASE);
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onClick(View view) {
            switch (view.getId()) {
                case R.id.blockchainProgressionButton:
                    findViewById(R.id.ioInterface).setVisibility(View.GONE);
                    findViewById(R.id.blockchainProgressionInterface).setVisibility(View.VISIBLE);
                    break;

            //transaction initiale vide qui initie le calcul de nonce
            Transaction transaction = new Transaction(0, contractIdentifier.address, Geth.newBigInt(0), 0, Geth.newBigInt(0), null);
            clientIdentifier.ethereumClient.sendTransaction(ctx, transaction);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

            //transaction initiale vide qui initie le calcul de nonce. échoue si nonce déjà initié.
            try {
                clientIdentifier.ethereumClient.sendTransaction(ctx, new Transaction(0,
                        contractIdentifier.address, Geth.newBigInt(0), 0, Geth.newBigInt(0),
                        null));
            } catch (Exception e) {
                //
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onClick(View view) {
            switch (view.getId()) {
                case R.id.blockchainProgressionButton:
                    findViewById(R.id.ioInterface).setVisibility(View.GONE);
                    findViewById(R.id.blockchainProgressionInterface).setVisibility(View.VISIBLE);
                    break;

                case R.id.ioInterfaceButton:
                    findViewById(R.id.ioInterface).setVisibility(View.VISIBLE);
                    findViewById(R.id.blockchainProgressionInterface).setVisibility(View.GONE);
                    break;

                case R.id.retrieveStandardsButton:
                    Interfaces retrieveStandards = null;
                    TextView textView = findViewById(R.id.textView10);
                    try {
                        retrieveStandards = retrieveStandards(ctx);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    textView.setText("");
                    BigInt[] array = new BigInt[(int)retrieveStandards.size()];
                    String[] params = new String[] {
                            "coLow",
                            "coHigh",
                            "co2Low",
                            "co2High",
                            "phLow",
                            "phHigh",
                            "turbLow",
                            "turbHigh"
                    };
                    for(int i = 0; i < retrieveStandards.size(); i++){
                        try {
                            array[i] = retrieveStandards.get(i).getUint16();
                            textView.append(params[i] + ": " + array[i].toString() + " ");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    break;

                case R.id.retrieveDataOneByOneButton:
                    Interfaces retrieveDataOneByOne = null;

                    TextView[] textViews = new TextView[] {
                            findViewById(R.id.textView43),
                            findViewById(R.id.textView44),
                            findViewById(R.id.textView45),
                            findViewById(R.id.textView46),
                            findViewById(R.id.textView47),
                            findViewById(R.id.textView48),
                            findViewById(R.id.textView49),
                            findViewById(R.id.textView50),
                    };

                    String[] strings = new String[] {
                            "country",
                            "location",
                            "deviceID",
                            "timeTTN",
                            "co",
                            "co2",
                            "ph",
                            "turbi",
                    };

                    try {
                        retrieveDataOneByOne = retrieveDataOneByOne(ctx, Integer.parseInt(index.getText().toString()));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        for(int i = 0; i < 3; i++){
                            textViews[i].setText(retrieveDataOneByOne.get(i).getString());
                        }
                        textViews[3].setText(strings[3] + " " +
                                retrieveDataOneByOne.get(3).getUint64().toString());
                        for(int i = 4; i < textViews.length; i++){
                            textViews[i].setText(strings[i] + " " +
                                    retrieveDataOneByOne.get(i).getUint16().toString());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;

                case R.id.updateStandardsAirButton:
                    TextView textView4 = findViewById(R.id.textView4);
                    textView4.setText("");

                    EditText[] editTexts = new EditText[] {
                            findViewById(R.id.textView39),
                            findViewById(R.id.textView40),
                            findViewById(R.id.textView41),
                            findViewById(R.id.textView42),
                    };

                    try {
                        Transaction updateStandardsAir = updateStandardsAir(ctx,
                                Integer.parseInt(editTexts[0].getText().toString()),
                                Integer.parseInt(editTexts[1].getText().toString()),
                                Integer.parseInt(editTexts[2].getText().toString()),
                                Integer.parseInt(editTexts[3].getText().toString()));
                        textView4.setText("Transaction envoyée !");
                    } catch (Exception e) {
                        textView4.setText("Transaction échouée !");
                        e.printStackTrace();
                    }
                    break;

                case R.id.updateStandardsWaterButton:
                    TextView textView5 = findViewById(R.id.textView5);
                    textView5.setText("");

                    EditText[] editTexts1 = new EditText[] {
                            findViewById(R.id.textView35),
                            findViewById(R.id.textView36),
                            findViewById(R.id.textView37),
                            findViewById(R.id.textView38),
                    };

                    try {
                        Transaction updateStandardsWater = updateStandardsWater(ctx,
                                Integer.parseInt(editTexts1[0].getText().toString()),
                                Integer.parseInt(editTexts1[1].getText().toString()),
                                Integer.parseInt(editTexts1[2].getText().toString()),
                                Integer.parseInt(editTexts1[3].getText().toString()));
                        textView5.setText("Transaction envoyée !");
                    } catch (Exception e) {
                        textView5.setText("Transaction échouée !");
                    }
                    break;

                default:
                    break;
            }
        }
}