package com.example.ethereumpassivenode;

import org.ethereum.geth.Enodes;
import org.ethereum.geth.Geth;
import org.ethereum.geth.NodeConfig;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import jnr.ffi.Struct;
import jnr.ffi.StructLayout;

public class NodeConfiguration {
    private static NodeConfiguration ourInstance;

    static {
        try {
            ourInstance = new NodeConfiguration();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static NodeConfiguration getInstance() {
        return ourInstance;
    }

    NodeConfig nodeConfig = new NodeConfig();

    private NodeConfiguration() throws Exception {
        //adresses de nodes statiques contactées par défaut
        Enodes enodes = Geth.newEnodes(1);
        enodes.set(0, Geth.newEnode(
                "enode://" + new String(Files.readAllBytes(Paths.get("/data/user/0/com.example.ethereumpassivenode/files/enode"))).substring(9, 137) + "@" + Network.primaryIpAddress + ":" + Network.enodeIpPort
        ));
        nodeConfig.setBootstrapNodes(enodes);

        //seules les nodes statiques peuvent se connecter
        nodeConfig.setMaxPeers(0);

        //allume le réseau ethereum
        nodeConfig.setEthereumEnabled(true);

        //id de réseau de node statique contactée
        nodeConfig.setEthereumNetworkID(6666);

        //genesis bloc de la node statique et de la node légère. il doit être identique pour
        //assurer la synchronisation
        nodeConfig.setEthereumGenesis("{\n" +
                "    \"config\": {\n" +
                "        \"chainId\": 1506754,\n" +
                "        \"homesteadBlock\": 0,\n" +
                "        \"eip155Block\": 0,\n" +
                "        \"eip158Block\": 0\n" +
                "    },\n" +
                "    \"difficulty\": \"20\",\n" +
                "    \"gasLimit\": \"2100000\",\n" +
                "    \"alloc\": {\n" +
                "        \"dafe06954170e684fbb0fed328a2933afed324d2\": {\n" +
                "            \"balance\": \"3000000\"\n" +
                "        }\n" +
                "    }\n" +
                "}");

        //taille de mémoire cache en MB. 16MB+
        nodeConfig.setEthereumDatabaseCache(16);

        //conneection netstats
        nodeConfig.setEthereumNetStats("gethdroid:biot@" + Network.primaryIpAddress + ":" + Network.netstatIpPort);

        //protocole whisper, échange de messages entre dapps
        nodeConfig.setWhisperEnabled(false);

        //pprof serveur
        nodeConfig.setPprofAddress("");
    }
}
