package com.example.ethereumpassivenode;

import android.annotation.TargetApi;
import android.os.AsyncTask;
import android.os.Build;

import androidx.annotation.RequiresApi;

import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

public class Network extends AsyncTask<String, Void, Boolean> {
    public static String[] ipAddressList = {
            "192.168.43.195",
            "192.168.43.185",
            "192.168.43.215",
    };
    public static String rpcIpPort = "8545";
    public static String wsIpPort = "8546";
    public static String netstatIpPort = "3010";
    public static String enodeIpPort = "30303";
    public static String webServerIpPort = "4200";
    public static String primaryIpAddress = "";

    protected Boolean doInBackground(String... params) {
        String url = params[0];
        int port = Integer.parseInt(params[1]);
        boolean success = false;

        try {
            success = pingURL(url, port);
        } catch (IOException e) {
            //e.printStackTrace();
        }

        return success;
    }

    protected void onPostExecute(Boolean result) {
        //
    }

    @TargetApi(19)
    public static boolean pingURL(String hostname, int port) throws IOException {
        boolean reachable = false;
        Socket socket = new Socket();

        try {
            socket.connect(new InetSocketAddress(hostname, port), 10);
            reachable = true;
        } catch (IOException e) {
            //e.printStackTrace();
        }
        return reachable;
    }

    public static void setPrimaryIpAddress(String url){
        primaryIpAddress = url;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public static void setBootstrapNode(String hostname, String port) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    InputStream inputStream = new URL("http://" + hostname + ":" + port + "/assets/enode_principal.txt").openStream();
                    Files.copy(inputStream, Paths.get("/data/user/0/com.example.ethereumpassivenode/files/enode"), StandardCopyOption.REPLACE_EXISTING);
                    inputStream.close();
                } catch (IOException e) {
                    ///e.printStackTrace();
                }
            }
        });
        thread.start();

    }

}
